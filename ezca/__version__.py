from .__scm_version__ import version, version_tuple
__version__ = version
__version_tuple__ = version_tuple
